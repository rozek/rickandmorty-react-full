import React, {useEffect, useState}from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'

import Nav from './components/navigation/Nav'
import Header from './components/ui/Header'
import About from './components/about/About'
import Characters from './components/characters/Characters'
import CharacterSpecification from './components/characters/CharacterSpecification'
import HandleReducer from './components/tools/HandleReducer'
// import Statistic from './components/statistics/Statistic'
import Statistic from './components/statistics/index'
import './App.css';

import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import { useSelector, useDispatch } from 'react-redux';

const useStyles = makeStyles({
  root: {
    maxWidth: 645,
  },
  media: {
    height: 140,
  },
  
});


function App() {
  return (
    <Router>
      <Nav /> 
    <div className="container">
      <Header /> 
      <Switch>
      <Route path='/' exact component={Home}/>
      <Route path='/about' component={About} /> 
      <Route path= '/characters' exact component={Characters} />
      <Route path = '/CharacterSpecification/:id' component={CharacterSpecification}/>
      <Route path = '/HandleReducer' component={HandleReducer}/>
      <Route path = '/Statistic' component={Statistic} />
      </Switch>
      {/* <Link to='/Statistic' component={Statistic}> I will navigate You to Statistic mate</Link> */}
      {/* <LocationDisplay /> */}
    </div>
    </Router>

  );
}

const Home = ()=>{
  const classes = useStyles();
  const counter = useSelector(state => state.counterReducer)
  const viewed = useSelector(state => state.countView)

 return (
  <div>
    <h1>Counter {counter}</h1>

    <Card className={classes.root}>
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image="/static/images/cards/contemplative-reptile.jpg"
          title="Contemplative Reptile"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            Lizard
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging
            across all continents except Antarctica
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button size="small" color="primary">
          Share
        </Button>
        <Button size="small" color="primary">
          Learn More
        </Button>
      </CardActions>
    </Card>
  </div>
)}

export default App;
