import React, {useState, useEffect } from 'react'
import './App.css'
import { Link } from 'react-router-dom'

const Shop = () => {

    useEffect(() => {
      fetchItems()
    }, [])

const [items, setItems] = useState([])

    const fetchItems = async () => {
        const data = await fetch('https://fortnite-api.theapinetwork.com/store/get')
        const items = await data.json()
        const itemsValues = items.data

        setItems(itemsValues)  
        console.log(itemsValues.map(item=> item.item.name));
        console.log(itemsValues)
    }
    return (
        <div>
            {items.map(item => 
                <h1 key={item.itemId}>
                    <Link to ={`/shop/${item.itemId}`}>{item.item.name}</Link>
                </h1>
                )}
        </div>
    )
}

export default Shop
