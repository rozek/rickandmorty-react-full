import counterReducer from './counter'
import loggedReducer from './isLogged'
import isFavouriteRedurec from './addFAV'
import countView from './countView'
import {combineReducers} from 'redux'

const allReducers = combineReducers({
  counterReducer,
  loggedReducer,
  isFavouriteRedurec,
  countView

})


export default allReducers;