import React, {useState, useEffect}from 'react'
import { useSelector} from 'react-redux';

import useFavsToLocal from '../hooks/useFavsToLocal'

import './Favourites.css';

const Favourites = () => {


    const favourites = useSelector(state => state.isFavouriteRedurec)
    // console.log("Favourites -> favourites", favourites)
    let favIDs = favourites.favIDs



    return (
        <div>
            {
            favIDs.map((favID)=> 

                <div className="favCards">  {favID}</div>

            )
            }
        </div>
    )
}

export default Favourites
