import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'

function getSavedValue(key, initialValue) {
  const savedValue = JSON.parse(localStorage.getItem(key))
  if (savedValue) return savedValue

  if (initialValue instanceof Function) return initialValue()
  return savedValue
}

const useFavsToLocal = (key, initialValue) => {
  const favourites = useSelector((state) => state.isFavouriteRedurec)
  // console.log("Favourites -> favourites", favourites)
  let favIDs = favourites.favIDs

  const [value, setValue] = useState(() => {
    return getSavedValue(key, initialValue)
  })

  useEffect(() => {
    let inStoreIDs = localStorage.getItem(key)
    console.log('useFavsToLocal -> inStoreIDs', inStoreIDs)
    localStorage.setItem(key, JSON.stringify(value))
  }, [value])

  return [value, setValue]
}

export default useFavsToLocal
