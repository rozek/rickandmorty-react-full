import React from 'react'
import { useSelector} from 'react-redux';


const HandleReducer = () => {

    const viewedState = useSelector(state => state.countView)
    const viewed = viewedState.count

// Counting values in array f.g([1,1,1,3,4,4,]  expect a:[1,3,4] b:[3times,1time,2times])

    function countView(arr) {
        var a = [], b = [], prev;
        
        arr.sort();
        for ( var i = 0; i < arr.length; i++ ) {
            if ( arr[i] !== prev ) {
                a.push(arr[i]);
                b.push(1);
            } else {
                b[b.length-1]++;
            }
            prev = arr[i];
        }
        
        return [a, b];
    }
    let result = countView(viewed)
    console.log(result);



// Merge arrays as a key/value

    function mergeArrays(arr1, arr2) {
        var l = Math.min(arr1.length,arr2.length),
                ret = [],
                i;
    
        for( i=0; i<l; i++) {
           ret.push(`Card no. ${arr1[i]} was clicked ${arr2[i]} times`);
        }
    
        return ret;
    }


const countedViews = mergeArrays(...result)
console.log("HandleReducer -> countedViews", countedViews)

   

      return (
        <div>
            <h2>
           Card Views stats:
              {countedViews.map(el => 
              
              <h2> {el} </h2>)}
            </h2>
        </div>
    )
  }
  
  export default HandleReducer