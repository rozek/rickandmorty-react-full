const mongoose = require('mongoose')

const PostSchema = mongoose.Schema({
    title: {
        type: Array,
        required: true
    },
    description:  {
        type: Array,
        required: true
    },
    user:  {
        type: String,
        required: true,
        default: 'anonimus'
    },
    date:  {
        type: Date,
        default: Date.now
    },
})

module.exports = mongoose.model('cardStats', PostSchema)